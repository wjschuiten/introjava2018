package nl.bioinf.nomi.hello;

public class Horse {
    int weightInKilograms;
    String color;

    void gallop(double speedInKmperHour) {
        System.out.println("Horse galloping at " + speedInKmperHour + " km per hour");
    }
}
